// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "PointDevice/PBR/High Standards_Metallic Model_Beta1_5"
{
	Properties
	{
		[NoScaleOffset]_MainTex("Albedo", 2D) = "white" {}
		_Color("Diffuse Coloring ", Color) = (1,1,1,0)
		[NoScaleOffset]_MetallicGlossMap("Metallic Smoothness", 2D) = "white" {}
		[Toggle]_PackedAO("PackedAO?", Float) = 1
		_Metallic("Metallic Multiply", Range( 0 , 1)) = 1
		_Glossiness("Smoothness Multiply", Range( 0 , 1)) = 1
		[NoScaleOffset][Normal]_BumpMap("Normal", 2D) = "bump" {}
		[NoScaleOffset]_OcclusionMap("Ambient Occlusion Map", 2D) = "white" {}
		[NoScaleOffset]_EmissionMap("Emissions Map", 2D) = "white" {}
		[HDR]_EmissionColor("Emissions Color", Color) = (1,1,1,0)
		_MIOR("Metallic IOR", Float) = 2.1
		_DIOR("Dielectric IOR", Float) = 1.6
		_DielectricSpecColoring("Dielectric Spec Coloring", Range( 0 , 1)) = 0.2
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _MainTex;
		uniform float4 _Color;
		uniform sampler2D _MetallicGlossMap;
		uniform float _Metallic;
		uniform float _DielectricSpecColoring;
		uniform sampler2D _BumpMap;
		uniform float _Glossiness;
		uniform float _PackedAO;
		uniform sampler2D _OcclusionMap;
		uniform float _DIOR;
		uniform float _MIOR;
		uniform sampler2D _EmissionMap;
		uniform float4 _EmissionColor;


		float MyCustomExpression290(  )
		{
			return unity_ColorSpaceDielectricSpec.a;
		}


		float3 EnergyConserve98( float3 albedo , float3 specColor )
		{
			 return albedo * (half3(1,1,1) - specColor);
		}


		half3 SHpericalHarmonicsLightDir12(  )
		{
			return float3(unity_SHAr.r,unity_SHAg.g,unity_SHAb.b);
		}


		float OrenNayarDiffuseCalc92( float3 N , float3 V , float3 L , float Roughness )
		{
			    float3 H = normalize(V+L);
			    float dotNL = saturate(dot(N,L));
			    float dotNV = saturate(dot(N,V));
			    float dotLH = saturate(dot(L,H));
			    float dotNH = saturate(dot(N,H));
			    float dotLH5 = pow(1.0-dotLH,5.);
			    
			    float theta_r = acos(dotNV);
				float theta_i = acos(dotNL);
				
				float alpha = max( theta_i, theta_r);
				float beta = min( theta_i, theta_r);
				
				float A = 1.0 - 0.5 * Roughness / (Roughness + 0.33);
				float B = 0.45 * Roughness / (Roughness + 0.09);
			    return saturate(dotNL) * (A + B * sin(alpha) * tan(beta));
		}


		float GGXSpecularCalc1( float3 L , float roughness , float F0 , float3 V , float3 N )
		{
			 float alpha = roughness*roughness;
			    float3 H = normalize(V+L);
			    float dotNL = dot(N,L);
			    float dotLH = dot(L,H);
			    float dotNH = dot(N,H);
				float dotVN = dot(V,N);
			    float F, D, vis;
			    // D
			    float alphaSqr = alpha*alpha;
			    float denom = dotNH * dotNH *(alphaSqr-1.0) + 1.0;
			    D = alphaSqr/(UNITY_PI * denom * denom);
			    // F
			    F = F0 + (1.-F0) * pow((1. - dotVN),5.);
			    //F = ;
			    // V
			    float k = alpha/2.;
			    float k2 = k*k;
			    float invK2 = 1.-k2;
			    vis = 1./(dotLH*dotLH*invK2 + k2);
			    float specular = dotNL * D * F * vis;
			    return specular;
			    //return vis;
		}


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, saturate( p - K.xxx ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		float FresnelCorrectedSmoothness361( float smoothness , float F0 , float3 V , float3 N )
		{
			    float dotVN = dot(V,N);
			    float F, S;
			    F = F0 + (1.-F0) * pow((1. - dotVN),5.);
			    S = max(smoothness,F);
			    return S;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float2 uv_MainTex48 = i.uv_texcoord;
			float4 break212 = ( tex2D( _MainTex, uv_MainTex48 ) * _Color );
			float3 appendResult52 = (float3(break212.r , break212.g , break212.b));
			float3 RawAlbedo247 = appendResult52;
			float localMyCustomExpression290 = MyCustomExpression290();
			float2 uv_MetallicGlossMap44 = i.uv_texcoord;
			float4 tex2DNode44 = tex2D( _MetallicGlossMap, uv_MetallicGlossMap44 );
			float Metallicmap49 = ( tex2DNode44.r * _Metallic );
			float3 AlbedoColor142 = ( RawAlbedo247 * ( localMyCustomExpression290 - ( localMyCustomExpression290 * Metallicmap49 ) ) );
			float3 albedo98 = AlbedoColor142;
			float3 temp_cast_0 = (_DielectricSpecColoring).xxx;
			float3 lerpResult50 = lerp( temp_cast_0 , RawAlbedo247 , Metallicmap49);
			float3 SpecularColor148 = lerpResult50;
			float3 specColor98 = SpecularColor148;
			float3 localEnergyConserve98 = EnergyConserve98( albedo98 , specColor98 );
			float2 uv_BumpMap59 = i.uv_texcoord;
			float3 WorldNormal72 = normalize( (WorldNormalVector( i , UnpackNormal( tex2D( _BumpMap, uv_BumpMap59 ) ) )) );
			float3 N92 = WorldNormal72;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ViewDir93 = ase_worldViewDir;
			float3 V92 = ViewDir93;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 break307 = max( ase_worldlightDir , float3( 0,0,0 ) );
			half3 localSHpericalHarmonicsLightDir12 = SHpericalHarmonicsLightDir12();
			float3 normalizeResult13 = normalize( localSHpericalHarmonicsLightDir12 );
			float3 temp_output_18_0 = max( normalizeResult13 , float3( 0,0,0 ) );
			float3 break299 = temp_output_18_0;
			float3 appendResult23 = (float3(-0.5 , 1.0 , -1.0));
			float3 normalizeResult24 = normalize( appendResult23 );
			float3 lightDir73 =  ( ( break307.x + break307.y + break307.z ) - 0.0 > 0.0 ? ase_worldlightDir : ( break307.x + break307.y + break307.z ) - 0.0 <= 0.0 && ( break307.x + break307.y + break307.z ) + 0.0 >= 0.0 ?  ( ( break299.x + break299.y + break299.z ) - 0.0 > 0.0 ? temp_output_18_0 : ( break299.x + break299.y + break299.z ) - 0.0 <= 0.0 && ( break299.x + break299.y + break299.z ) + 0.0 >= 0.0 ? normalizeResult24 : temp_output_18_0 )  : ase_worldlightDir ) ;
			float3 L92 = lightDir73;
			float Smoothness160 = ( tex2DNode44.a * _Glossiness );
			float clampResult46 = clamp( ( 1.0 - saturate( Smoothness160 ) ) , 0.01 , 0.99 );
			float Roughness101 = clampResult46;
			float Roughness92 = Roughness101;
			float localOrenNayarDiffuseCalc92 = OrenNayarDiffuseCalc92( N92 , V92 , L92 , Roughness92 );
			float2 uv_OcclusionMap206 = i.uv_texcoord;
			float4 tex2DNode206 = tex2D( _OcclusionMap, uv_OcclusionMap206 );
			float AmbientOcclusion193 = saturate( (( _PackedAO )?( tex2DNode206.g ):( tex2DNode206.r )) );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float LightIntensity333 = ase_lightColor.a;
			float ifLocalVar313 = 0;
			if( LightIntensity333 > 0.0 )
				ifLocalVar313 = ase_lightAtten;
			else if( LightIntensity333 == 0.0 )
				ifLocalVar313 = 1.0;
			float LightAttenuation314 = ifLocalVar313;
			float temp_output_229_0 = ( AmbientOcclusion193 * LightAttenuation314 );
			float DiffuseTerm183 = ( localOrenNayarDiffuseCalc92 * temp_output_229_0 );
			float3 Diffuse70 = ( localEnergyConserve98 * DiffuseTerm183 );
			float3 L1 = lightDir73;
			float roughness1 = Roughness101;
			float lerpResult293 = lerp( _DIOR , _MIOR , Metallicmap49);
			float temp_output_336_0 = ( lerpResult293 - 1.0 );
			float temp_output_337_0 = ( lerpResult293 + 1.0 );
			float CombinedIOR96 = ( ( temp_output_336_0 * temp_output_336_0 ) / ( temp_output_337_0 * temp_output_337_0 ) );
			float F01 = CombinedIOR96;
			float3 V1 = ViewDir93;
			float3 N1 = WorldNormal72;
			float localGGXSpecularCalc1 = GGXSpecularCalc1( L1 , roughness1 , F01 , V1 , N1 );
			float3 hsvTorgb322 = RGBToHSV( ase_lightColor.rgb );
			float clampResult334 = clamp( hsvTorgb322.y , 0.0 , 0.9 );
			float ifLocalVar326 = 0;
			if( hsvTorgb322.z <= 0.1 )
				ifLocalVar326 = 1.0;
			else
				ifLocalVar326 = hsvTorgb322.z;
			float LightColorValue315 = ifLocalVar326;
			float3 hsvTorgb321 = HSVToRGB( float3(hsvTorgb322.x,clampResult334,LightColorValue315) );
			float3 LightColor136 = hsvTorgb321;
			float Shadows356 = temp_output_229_0;
			float3 SpecularCalcAndColor155 = ( ( SpecularColor148 * localGGXSpecularCalc1 ) * LightColor136 * Shadows356 );
			float3 indirectNormal106 = WorldNormal72;
			float smoothness361 = Smoothness160;
			float F0361 = CombinedIOR96;
			float3 V361 = ViewDir93;
			float3 N361 = WorldNormal72;
			float localFresnelCorrectedSmoothness361 = FresnelCorrectedSmoothness361( smoothness361 , F0361 , V361 , N361 );
			float dotResult370 = dot( WorldNormal72 , lightDir73 );
			float lerpResult376 = lerp( saturate( dotResult370 ) , 1.0 , Metallicmap49);
			Unity_GlossyEnvironmentData g106 = UnityGlossyEnvironmentSetup( localFresnelCorrectedSmoothness361, data.worldViewDir, indirectNormal106, float3(0,0,0));
			float3 indirectSpecular106 = UnityGI_IndirectSpecular( data, (0.1 + (( Shadows356 * lerpResult376 ) - 0.0) * (1.0 - 0.1) / (1.0 - 0.0)), indirectNormal106, g106 );
			float3 Reflections157 = ( indirectSpecular106 * SpecularColor148 );
			UnityGI gi10 = gi;
			float3 diffNorm10 = WorldNormal72;
			gi10 = UnityGI_Base( data, 1, diffNorm10 );
			float3 indirectDiffuse10 = gi10.indirect.diffuse + diffNorm10 * 0.0001;
			float3 IndirectDiff228 = indirectDiffuse10;
			float2 uv_EmissionMap217 = i.uv_texcoord;
			float4 break221 = ( tex2D( _EmissionMap, uv_EmissionMap217 ) * _EmissionColor );
			float3 appendResult220 = (float3(break221.r , break221.g , break221.b));
			float3 Emssions222 = appendResult220;
			c.rgb = ( ( ( Diffuse70 + SpecularCalcAndColor155 + Reflections157 + ( IndirectDiff228 * AlbedoColor142 * LightColor136 ) ) * LightColor136 ) + Emssions222 );
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
1920;24;1920;1034;2305.786;1507.68;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;181;-2340.492,-2932.729;Inherit;False;4839.28;2546.523;;4;180;173;179;172;Release Canidate Beta Version 1_5. Made By PointDevice. Please See README file.;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;172;-353.4719,-2867.406;Inherit;False;1499.016;1244.708;Data Indulge;26;219;217;218;206;72;193;5;59;160;49;208;207;44;210;211;213;52;212;214;48;221;220;222;247;358;359;;0,1,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;179;-2296.468,-2859.589;Inherit;False;1928.842;2425.956;Common Values ;11;177;171;178;175;174;310;311;312;313;314;176;;1,0,0,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;210;-31.63544,-2238.694;Inherit;False;Property;_Metallic;Metallic Multiply;4;0;Create;False;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;48;-303.4719,-2817.406;Inherit;True;Property;_MainTex;Albedo;0;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;None;a87c7264cc8df8249bd4e5eb8f9e6ff4;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;213;-216.4625,-2634.458;Inherit;False;Property;_Color;Diffuse Coloring ;1;0;Create;False;0;0;False;0;False;1,1,1,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;44;-310.0225,-2221.928;Inherit;True;Property;_MetallicGlossMap;Metallic Smoothness;2;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;None;f352576f324f9ad48bd0a1ac2786a95a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;175;-2229.727,-2040.916;Inherit;False;1850.547;436.2612;Light Direction Calc and Fallback;14;73;28;300;24;23;299;18;13;12;307;309;308;302;304;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;208;228.3601,-2224.398;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;12;-2207.425,-1856.292;Half;False;return float3(unity_SHAr.r,unity_SHAg.g,unity_SHAb.b)@;3;False;0;SHperical Harmonics Light Dir;True;False;0;0;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;214;44.54062,-2811.56;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;171;-2233.091,-1595.196;Inherit;False;1399.755;440.2959;Metallic To Specular;11;142;148;50;51;144;253;289;288;290;260;349;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;49;352.8947,-2225.248;Inherit;False;Metallicmap;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;13;-1953.096,-1857.855;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;212;166.3775,-2812.758;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMaxOpNode;18;-1815.767,-1860.501;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;52;392.7208,-2812.148;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;302;-1832.091,-1754.319;Inherit;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;51;-2201.279,-1327.564;Inherit;False;49;Metallicmap;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;176;-2228.292,-2406.781;Inherit;False;1566.357;359.6511;Light Color And Fallback;10;136;322;321;326;315;327;111;332;333;334;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CustomExpressionNode;290;-1980.789,-1341.068;Inherit;False;return unity_ColorSpaceDielectricSpec.a@;1;False;0;My Custom Expression;True;False;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;309;-1518.943,-1720.05;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;211;-32.63543,-2147.694;Inherit;False;Property;_Glossiness;Smoothness Multiply;5;0;Create;False;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;247;514.2511,-2812.563;Inherit;False;RawAlbedo;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightColorNode;111;-2189.178,-2371.358;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.BreakToComponentsNode;299;-1682.865,-1893.694;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.DynamicAppendNode;23;-1658.686,-2005.588;Inherit;False;FLOAT3;4;0;FLOAT;-0.5;False;1;FLOAT;1;False;2;FLOAT;-1;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;289;-1743.831,-1286.169;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;333;-1994.605,-2318.203;Inherit;False;LightIntensity;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;206;-309.5164,-2025.14;Inherit;True;Property;_OcclusionMap;Ambient Occlusion Map;7;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;None;f352576f324f9ad48bd0a1ac2786a95a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;144;-2197.815,-1396.084;Inherit;False;247;RawAlbedo;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;207;225.6841,-2105.267;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;300;-1449.865,-1893.694;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;288;-1616.494,-1339.295;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;307;-1358.943,-1725.05;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;260;-2219.104,-1548.974;Inherit;False;Property;_DielectricSpecColoring;Dielectric Spec Coloring;12;0;Create;True;0;0;False;0;False;0.2;0.2;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;24;-1467.836,-1972.402;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;312;-2081.658,-678.0104;Inherit;False;333;LightIntensity;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;253;-1474.654,-1394.104;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;310;-2077.786,-605.7307;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;311;-2074.862,-540.6251;Inherit;False;Constant;_Float1;Float 1;13;0;Create;True;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;359;157.9755,-1982.825;Inherit;False;Property;_PackedAO;PackedAO?;3;0;Create;True;0;0;False;0;False;1;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;160;351.8799,-2105.643;Inherit;False;Smoothness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;177;-1276.813,-2805.73;Inherit;False;907.3809;324.8863;IOR Masking;11;293;42;335;294;96;336;337;338;339;340;341;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;178;-2226.047,-2809.589;Inherit;False;925.5498;396.0186;Commonly Used Values and Unit Conversion;7;161;91;45;46;101;3;93;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;173;-356.2255,-1607.631;Inherit;False;2778.183;1173.319;Lighting Calculations ;10;169;170;146;205;147;204;203;163;165;216;;0,0,1,1;0;0
Node;AmplifyShaderEditor.LerpOp;50;-1940.887,-1549.196;Inherit;False;3;0;FLOAT3;0.1,0.1,0.1;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;308;-1114.168,-1725.171;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;59;-311.6879,-1821.671;Inherit;True;Property;_BumpMap;Normal;6;2;[NoScaleOffset];[Normal];Create;False;0;0;False;0;False;-1;None;eb1c8d1e1aa30334a8a6468764081f94;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCIf;28;-1215.478,-1939.986;Inherit;False;6;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;358;542.4113,-1952.175;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;148;-1755.601,-1550.985;Inherit;False;SpecularColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;161;-2176.047,-2569.85;Inherit;False;160;Smoothness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;42;-1263.312,-2770.73;Inherit;False;Property;_DIOR;Dielectric IOR;11;0;Create;False;0;0;False;0;False;1.6;1.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;165;-305.8732,-1122.596;Inherit;False;667.7346;375.7114;Energy Conservation Calc;5;242;244;98;143;149;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;294;-1264.87,-2629.65;Inherit;False;49;Metallicmap;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCIf;304;-861.696,-1892.991;Inherit;False;6;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;335;-1264.238,-2703.059;Inherit;False;Property;_MIOR;Metallic IOR;10;0;Create;False;0;0;False;0;False;2.1;2.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;5;-31.51874,-1816.183;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ConditionalIfNode;313;-1749.658,-667.0104;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;142;-1312.407,-1450.914;Inherit;False;AlbedoColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;174;-2234.356,-1150.606;Inherit;False;1795.138;429.3586;Indirect Specular lighitng;19;256;157;109;106;159;107;189;357;360;362;363;364;365;361;370;372;371;375;376;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;216;311.2111,-1273.468;Inherit;False;898.7719;208.7219;Mix Ambient Occlusion WIth Diffuse;6;183;229;318;199;355;356;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;314;-1554.738,-672.382;Inherit;False;LightAttenuation;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;73;-617.9017,-1897.548;Inherit;False;lightDir;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;193;681.803,-1955.729;Inherit;False;AmbientOcclusion;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;293;-1070.87,-2756.65;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;149;-255.8733,-1002.479;Inherit;False;148;SpecularColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;91;-1981.137,-2568.145;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;72;143.9992,-1814.483;Inherit;False;WorldNormal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;143;-240.3941,-1072.597;Inherit;False;142;AlbedoColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;327;-2172.216,-2254.351;Inherit;False;Constant;_Float0;Float 0;12;0;Create;True;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;199;349.2111,-1205.968;Inherit;False;193;AmbientOcclusion;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;371;-2180.261,-802.4116;Inherit;False;73;lightDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;365;-2205.2,-916.6171;Inherit;False;72;WorldNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;318;350.2098,-1140.166;Inherit;False;314;LightAttenuation;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;45;-1841.27,-2567.457;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;336;-922.3212,-2775.34;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;98;141.3847,-1063.021;Inherit;False; return albedo * (half3(1,1,1) - specColor)@;3;False;2;True;albedo;FLOAT3;0,0,0;In;;Inherit;False;True;specColor;FLOAT3;0,0,0;In;;Inherit;False;EnergyConserve;True;False;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RGBToHSVNode;322;-1999.964,-2241.022;Inherit;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;337;-917.3212,-2682.34;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;338;-805.3212,-2684.34;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;229;581.4315,-1191.445;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;203;1488.169,-1033.475;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ConditionalIfNode;326;-1720.215,-2340.351;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;0.1;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;46;-1679.645,-2569.571;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.01;False;2;FLOAT;0.99;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;340;-792.3212,-2779.34;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;370;-1907.261,-875.4116;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;3;-2163.663,-2759.589;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;356;750.5444,-1138.426;Inherit;False;Shadows;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;375;-1830.617,-790.8346;Inherit;False;49;Metallicmap;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;163;-306.2255,-1557.632;Inherit;False;607.7173;432.4623;Oren Nayar Diffuse Calc;7;92;94;76;75;132;353;354;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SaturateNode;360;-1790.366,-875.8654;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;339;-646.3212,-2731.34;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;315;-1542.564,-2337.669;Inherit;False;LightColorValue;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;204;1489.169,-1033.475;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;101;-1543.498,-2570.114;Inherit;False;Roughness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;93;-1988.744,-2731.21;Inherit;False;ViewDir;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;170;610.2745,-970.1992;Inherit;False;1291.554;520.2374;GGX specular Calc and Coloring;15;245;155;138;137;243;57;1;150;151;152;154;153;74;291;292;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;205;1489.169,-1030.475;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;189;-1938.277,-946.603;Inherit;False;356;Shadows;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;334;-1720.361,-2161.386;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.9;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;75;-301.2255,-1504.632;Inherit;False;72;WorldNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;132;-288.5789,-1297.169;Inherit;False;101;Roughness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;96;-573.431,-2592.164;Inherit;False;CombinedIOR;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;76;-270.3895,-1364.94;Inherit;False;73;lightDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;147;1485.936,-1287.431;Inherit;False;219;183;Diffuse Coloring ;4;125;202;201;200;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;94;-272.1815,-1434.306;Inherit;False;93;ViewDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;332;-2197.978,-2133.658;Inherit;False;315;LightColorValue;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;376;-1629.292,-877.2622;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;364;-2205.063,-978.2484;Inherit;False;93;ViewDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;153;662.2745,-638.3884;Inherit;False;93;ViewDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;202;1489.618,-1204.636;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;74;669.4653,-844.691;Inherit;False;73;lightDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomExpressionNode;92;98.49182,-1452.432;Inherit;False;    float3 H = normalize(V+L)@$$    float dotNL = saturate(dot(N,L))@$    float dotNV = saturate(dot(N,V))@$    float dotLH = saturate(dot(L,H))@$    float dotNH = saturate(dot(N,H))@$    float dotLH5 = pow(1.0-dotLH,5.)@$    $    float theta_r = acos(dotNV)@$	float theta_i = acos(dotNL)@$	$	float alpha = max( theta_i, theta_r)@$	float beta = min( theta_i, theta_r)@$	$	float A = 1.0 - 0.5 * Roughness / (Roughness + 0.33)@$	float B = 0.45 * Roughness / (Roughness + 0.09)@$$    return saturate(dotNL) * (A + B * sin(alpha) * tan(beta))@;1;False;4;True;N;FLOAT3;0,0,0;In;;Inherit;False;True;V;FLOAT3;0,0,0;In;;Inherit;False;True;L;FLOAT3;0,0,0;In;;Inherit;False;True;Roughness;FLOAT;0;In;;Inherit;False;Oren Nayar Diffuse Calc;True;False;0;4;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;151;661.3845,-708.6023;Inherit;False;96;CombinedIOR;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;372;-1437.261,-943.4116;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;154;660.2745,-571.3884;Inherit;False;72;WorldNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;363;-2206.063,-1042.248;Inherit;False;96;CombinedIOR;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;362;-2207.063,-1103.248;Inherit;False;160;Smoothness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.HSVToRGBNode;321;-1466.964,-2180.022;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;152;664.6039,-777.2415;Inherit;False;101;Roughness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;201;1490.618,-1203.636;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomExpressionNode;361;-1944.213,-1081.388;Inherit;False;    float dotVN = dot(V,N)@$$    float F, S@$$    F = F0 + (1.-F0) * pow((1. - dotVN),5.)@$    S = max(smoothness,F)@$    return S@;1;False;4;True;smoothness;FLOAT;0;In;;Inherit;False;True;F0;FLOAT;1;In;;Inherit;False;True;V;FLOAT3;0,0,0;In;;Inherit;False;True;N;FLOAT3;0,0,0;In;;Inherit;False;Fresnel Corrected Smoothness;True;False;0;4;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;355;754.7242,-1235.343;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;150;843.8949,-866.5658;Inherit;False;148;SpecularColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;136;-868.0458,-2244.062;Inherit;False;LightColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;146;495.5217,-1568.134;Inherit;False;1306.293;267.6931;Ambient Light;6;10;123;139;134;145;228;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CustomExpressionNode;1;907.018,-751.8569;Inherit;False; float alpha = roughness*roughness@$$    float3 H = normalize(V+L)@$$    float dotNL = dot(N,L)@$$    float dotLH = dot(L,H)@$    float dotNH = dot(N,H)@$	float dotVN = dot(V,N)@$$    float F, D, vis@$$    // D$    float alphaSqr = alpha*alpha@$    float denom = dotNH * dotNH *(alphaSqr-1.0) + 1.0@$    D = alphaSqr/(UNITY_PI * denom * denom)@$$    // F$    F = F0 + (1.-F0) * pow((1. - dotVN),5.)@$    //F = @$$    // V$    float k = alpha/2.@$    float k2 = k*k@$    float invK2 = 1.-k2@$    vis = 1./(dotLH*dotLH*invK2 + k2)@$$    float specular = dotNL * D * F * vis@$    return specular@$    //return vis@;1;False;5;True;L;FLOAT3;0,0,0;In;;Inherit;False;True;roughness;FLOAT;0;In;;Inherit;False;True;F0;FLOAT;1;In;;Inherit;False;True;V;FLOAT3;0,0,0;In;;Inherit;False;True;N;FLOAT3;0,0,0;In;;Inherit;False;GGX Specular Calc;True;False;0;5;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;357;-1303.815,-946.576;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0.1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;107;-1567.343,-1104.087;Inherit;False;72;WorldNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;243;1176.681,-611.8429;Inherit;False;356;Shadows;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.IndirectSpecularLight;106;-1074.027,-1098.9;Inherit;False;World;3;0;FLOAT3;0,0,1;False;1;FLOAT;1;False;2;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;200;1492.618,-1203.636;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;183;955.7493,-1234.893;Inherit;False;DiffuseTerm;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;123;511.5215,-1519.134;Inherit;False;72;WorldNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;137;1179.344,-679.0972;Inherit;False;136;LightColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;1260.408,-769.4434;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;218;84.25349,-2414.985;Inherit;False;Property;_EmissionColor;Emissions Color;9;1;[HDR];Create;False;0;0;False;0;False;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;256;-1025.1,-943.6028;Inherit;False;148;SpecularColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;217;-4.345317,-2600.215;Inherit;True;Property;_EmissionMap;Emissions Map;8;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;138;1443.533,-765.9662;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;169;2092.521,-1556.36;Inherit;False;293;165;Final Diffuse output;1;70;;1,1,1,1;0;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;10;715.1182,-1517.647;Inherit;False;World;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;109;-802.335,-960.9626;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;125;1535.936,-1237.431;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;219;325.778,-2594.061;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;221;449.9988,-2595.63;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RegisterLocalVarNode;70;2142.521,-1506.36;Inherit;False;Diffuse;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;155;1577.698,-765.6313;Inherit;False;SpecularCalcAndColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;228;948.6799,-1522.941;Inherit;False;IndirectDiff;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;157;-660.7618,-964.4784;Inherit;False;Reflections;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;145;873.5959,-1449.805;Inherit;False;142;AlbedoColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;180;1157.888,-2116.678;Inherit;False;1259.38;490.7773;Final Shading Output;10;99;156;158;71;0;191;223;226;374;373;;1,0,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;139;877.0129,-1382.441;Inherit;False;136;LightColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;71;1248.532,-1928.332;Inherit;False;70;Diffuse;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;156;1179.8,-1845.596;Inherit;False;155;SpecularCalcAndColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;134;1172.261,-1516.257;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;220;673.9988,-2594.63;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;158;1215.036,-1771.309;Inherit;False;157;Reflections;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;99;1471.767,-1878.705;Inherit;False;4;4;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;191;1456.259,-1737.692;Inherit;False;136;LightColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;222;794.9988,-2593.63;Inherit;False;Emssions;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;223;1805.265,-1813.728;Inherit;False;222;Emssions;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;373;1676.176,-1879.148;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;242;-245.1782,-926.9701;Inherit;False;228;IndirectDiff;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;374;1805.206,-1741.931;Inherit;False;96;CombinedIOR;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;349;-2198.218,-1464.127;Inherit;False;return unity_ColorSpaceDielectricSpec.rgb@;3;False;0;My Custom Expression;True;False;0;0;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;292;851.8218,-931.2697;Inherit;False;228;IndirectDiff;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;341;-996.7213,-2566.54;Inherit;False;Constant;_Float2;Float 2;13;0;Create;True;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;245;1176.818,-545.6545;Inherit;False;314;LightAttenuation;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;291;1066.597,-906.5535;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;159;-1334.486,-1110.406;Inherit;False;160;Smoothness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;226;1996.265,-1878.728;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;354;57.79041,-1240.687;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;244;-0.4393387,-999.7315;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;353;-289.2096,-1222.687;Inherit;False;49;Metallicmap;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2154.267,-2066.678;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;PointDevice/PBR/High Standards_Metallic Model_Beta1_5;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;208;0;44;1
WireConnection;208;1;210;0
WireConnection;214;0;48;0
WireConnection;214;1;213;0
WireConnection;49;0;208;0
WireConnection;13;0;12;0
WireConnection;212;0;214;0
WireConnection;18;0;13;0
WireConnection;52;0;212;0
WireConnection;52;1;212;1
WireConnection;52;2;212;2
WireConnection;309;0;302;0
WireConnection;247;0;52;0
WireConnection;299;0;18;0
WireConnection;289;0;290;0
WireConnection;289;1;51;0
WireConnection;333;0;111;2
WireConnection;207;0;44;4
WireConnection;207;1;211;0
WireConnection;300;0;299;0
WireConnection;300;1;299;1
WireConnection;300;2;299;2
WireConnection;288;0;290;0
WireConnection;288;1;289;0
WireConnection;307;0;309;0
WireConnection;24;0;23;0
WireConnection;253;0;144;0
WireConnection;253;1;288;0
WireConnection;359;0;206;1
WireConnection;359;1;206;2
WireConnection;160;0;207;0
WireConnection;50;0;260;0
WireConnection;50;1;144;0
WireConnection;50;2;51;0
WireConnection;308;0;307;0
WireConnection;308;1;307;1
WireConnection;308;2;307;2
WireConnection;28;0;300;0
WireConnection;28;2;18;0
WireConnection;28;3;24;0
WireConnection;28;4;18;0
WireConnection;358;0;359;0
WireConnection;148;0;50;0
WireConnection;304;0;308;0
WireConnection;304;2;302;0
WireConnection;304;3;28;0
WireConnection;304;4;302;0
WireConnection;5;0;59;0
WireConnection;313;0;312;0
WireConnection;313;2;310;0
WireConnection;313;3;311;0
WireConnection;142;0;253;0
WireConnection;314;0;313;0
WireConnection;73;0;304;0
WireConnection;193;0;358;0
WireConnection;293;0;42;0
WireConnection;293;1;335;0
WireConnection;293;2;294;0
WireConnection;91;0;161;0
WireConnection;72;0;5;0
WireConnection;45;0;91;0
WireConnection;336;0;293;0
WireConnection;98;0;143;0
WireConnection;98;1;149;0
WireConnection;322;0;111;1
WireConnection;337;0;293;0
WireConnection;338;0;337;0
WireConnection;338;1;337;0
WireConnection;229;0;199;0
WireConnection;229;1;318;0
WireConnection;203;0;98;0
WireConnection;326;0;322;3
WireConnection;326;2;322;3
WireConnection;326;3;327;0
WireConnection;326;4;327;0
WireConnection;46;0;45;0
WireConnection;340;0;336;0
WireConnection;340;1;336;0
WireConnection;370;0;365;0
WireConnection;370;1;371;0
WireConnection;356;0;229;0
WireConnection;360;0;370;0
WireConnection;339;0;340;0
WireConnection;339;1;338;0
WireConnection;315;0;326;0
WireConnection;204;0;203;0
WireConnection;101;0;46;0
WireConnection;93;0;3;0
WireConnection;205;0;204;0
WireConnection;334;0;322;2
WireConnection;96;0;339;0
WireConnection;376;0;360;0
WireConnection;376;2;375;0
WireConnection;202;0;205;0
WireConnection;92;0;75;0
WireConnection;92;1;94;0
WireConnection;92;2;76;0
WireConnection;92;3;132;0
WireConnection;372;0;189;0
WireConnection;372;1;376;0
WireConnection;321;0;322;1
WireConnection;321;1;334;0
WireConnection;321;2;332;0
WireConnection;201;0;202;0
WireConnection;361;0;362;0
WireConnection;361;1;363;0
WireConnection;361;2;364;0
WireConnection;361;3;365;0
WireConnection;355;0;92;0
WireConnection;355;1;229;0
WireConnection;136;0;321;0
WireConnection;1;0;74;0
WireConnection;1;1;152;0
WireConnection;1;2;151;0
WireConnection;1;3;153;0
WireConnection;1;4;154;0
WireConnection;357;0;372;0
WireConnection;106;0;107;0
WireConnection;106;1;361;0
WireConnection;106;2;357;0
WireConnection;200;0;201;0
WireConnection;183;0;355;0
WireConnection;57;0;150;0
WireConnection;57;1;1;0
WireConnection;138;0;57;0
WireConnection;138;1;137;0
WireConnection;138;2;243;0
WireConnection;10;0;123;0
WireConnection;109;0;106;0
WireConnection;109;1;256;0
WireConnection;125;0;200;0
WireConnection;125;1;183;0
WireConnection;219;0;217;0
WireConnection;219;1;218;0
WireConnection;221;0;219;0
WireConnection;70;0;125;0
WireConnection;155;0;138;0
WireConnection;228;0;10;0
WireConnection;157;0;109;0
WireConnection;134;0;228;0
WireConnection;134;1;145;0
WireConnection;134;2;139;0
WireConnection;220;0;221;0
WireConnection;220;1;221;1
WireConnection;220;2;221;2
WireConnection;99;0;71;0
WireConnection;99;1;156;0
WireConnection;99;2;158;0
WireConnection;99;3;134;0
WireConnection;222;0;220;0
WireConnection;373;0;99;0
WireConnection;373;1;191;0
WireConnection;291;0;292;0
WireConnection;291;1;150;0
WireConnection;226;0;373;0
WireConnection;226;1;223;0
WireConnection;354;2;353;0
WireConnection;244;0;149;0
WireConnection;244;1;242;0
WireConnection;0;13;226;0
ASEEND*/
//CHKSM=043A844547F84E3D2BBBE4986ABC870F644D8DD1