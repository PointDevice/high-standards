// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "PointDevice/PBR High Standards/Specular Model Beta1_8 RC_1 pre release"
{
	Properties
	{
		[NoScaleOffset]_MainTex("Diffuse", 2D) = "white" {}
		_Color("Diffuse Coloring ", Color) = (1,1,1,0)
		[NoScaleOffset]_SpecGlossMap("Specular Gloss map", 2D) = "white" {}
		_SpecColor("SpecualrColoring", Color) = (1,1,1,0)
		[Toggle]_UseInBuiltEnergyConservation("Use InBuilt Energy Conservation", Float) = 1
		[Toggle]_PackedAO("PackedAO?", Float) = 0
		_Glossiness("Gloss Multiply", Range( 0 , 1)) = 1
		[NoScaleOffset][Normal]_BumpMap("Normal", 2D) = "bump" {}
		[NoScaleOffset]_OcclusionMap("Ambient Occlusion Map", 2D) = "white" {}
		[NoScaleOffset]_EmissionMap("Emissions Map", 2D) = "white" {}
		[HDR]_EmissionColor("Emissions Color", Color) = (1,1,1,0)
		_DIOR("IOR", Float) = 1.45
		_FixedFallBackLightDir("Fixed Fall Back Light Dir", Vector) = (-0.5,1,-1,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldNormal;
			INTERNAL_DATA
			float2 uv_texcoord;
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _BumpMap;
		uniform float3 _FixedFallBackLightDir;
		uniform sampler2D _SpecGlossMap;
		uniform float _Glossiness;
		uniform float _PackedAO;
		uniform sampler2D _OcclusionMap;
		uniform sampler2D _MainTex;
		uniform float4 _Color;
		uniform float _UseInBuiltEnergyConservation;
		uniform float _DIOR;
		uniform sampler2D _EmissionMap;
		uniform float4 _EmissionColor;


		float3 HSVToRGB( float3 c )
		{
			float4 K = float4( 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 );
			float3 p = abs( frac( c.xxx + K.xyz ) * 6.0 - K.www );
			return c.z * lerp( K.xxx, saturate( p - K.xxx ), c.y );
		}


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		half3 SHpericalHarmonicsLightDir12(  )
		{
			return float3(unity_SHAr.r,unity_SHAg.g,unity_SHAb.b);
		}


		float OrenNayarDiffuseCalc92( float3 N , float3 V , float3 L , float Roughness )
		{
			    float3 H = normalize(V+L);
			    float dotNL = saturate(dot(N,L));
			    float dotNV = saturate(dot(N,V));
			    float dotLH = saturate(dot(L,H));
			    float dotNH = saturate(dot(N,H));
			    float dotLH5 = pow(1.0-dotLH,5.);
			    
			    float theta_r = acos(dotNV);
				float theta_i = acos(dotNL);
				
				float alpha = max( theta_i, theta_r);
				float beta = min( theta_i, theta_r);
				
				float A = 1.0 - 0.5 * Roughness / (Roughness + 0.33);
				float B = 0.45 * Roughness / (Roughness + 0.09);
			    return saturate(dotNL) * (A + B * sin(alpha) * tan(beta));
		}


		float GGXSpecularCalc1( float3 L , float roughness , float F0 , float3 V , float3 N )
		{
			 float alpha = roughness*roughness;
			    L = normalize(L);
			    float3 H = normalize(V+L);
			    float dotNL = dot(N,L);
			    float dotLH = dot(L,H);
			    float dotNH = dot(N,H);
				float dotVN = dot(V,N);
			    float F, D, vis;
			    // D
			    float alphaSqr = alpha*alpha;
			    float denom = dotNH * dotNH *(alphaSqr-1.0) + 1.0;
			    D = alphaSqr/(UNITY_PI * denom * denom);
			    // F
			    F = F0 + (1.-F0) * pow((1. - dotVN),5.);
			    // V
			    float k = alpha/2.;
			    float k2 = k*k;
			    float invK2 = 1.-k2;
			    vis = 1./(dotLH*dotLH*invK2 + k2);
			    //vis = (1./(dotNL*(1.-k)+k))*(1./(dotVN*(1.-k)+k));
			    float specular = dotNL * D * F * vis;
			    return specular;
			    //return vis;
		}


		float FresnelCorrectedSmoothness361( float smoothness , float F0 , float3 V , float3 N )
		{
			    float dotVN = dot(V,N);
			    float F, S;
			    F = F0 + (1.-F0) * pow((1. - dotVN),5.);
			    S = max(smoothness,F);
			    return S;
		}


		float SurfaceReduction435( float roughness )
		{
			    float surfaceReduction;
			#   ifdef UNITY_COLORSPACE_GAMMA
			        surfaceReduction = 1.0-0.28*roughness*roughness*roughness;
			#   else
			        surfaceReduction = 1.0 / (roughness*roughness + 1.0);
			#   endif
			return surfaceReduction;
		}


		float3 FowardBasePass460( float3 indirrectDiff , float3 lightColor , float diffTerm , float3 diffColor , float specularTerm , float3 specColor , float3 reflections , float surfaceReduction )
		{
			return  diffColor* ((lightColor * diffTerm ) + indirrectDiff)
			+ specularTerm * lightColor * specColor
			+surfaceReduction * reflections * specColor;
		}


		float3 ForwardAddPass541( float3 diffColor , float3 lightColor , float diffTerm , float3 ambient , float specularTerm , float3 specColor , float attenuation )
		{
			return  ((diffColor* (lightColor * diffTerm))
			+ specularTerm * lightColor * specColor) * attenuation;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float2 uv_BumpMap59 = i.uv_texcoord;
			float3 tex2DNode59 = UnpackNormal( tex2D( _BumpMap, uv_BumpMap59 ) );
			float3 WorldNormal72 = normalize( (WorldNormalVector( i , tex2DNode59 )) );
			UnityGI gi10 = gi;
			float3 diffNorm10 = WorldNormal72;
			gi10 = UnityGI_Base( data, 1, diffNorm10 );
			float3 indirectDiffuse10 = gi10.indirect.diffuse + diffNorm10 * 0.0001;
			float3 IndirectDiff228 = indirectDiffuse10;
			float3 indirrectDiff460 = IndirectDiff228;
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float3 hsvTorgb322 = RGBToHSV( ase_lightColor.rgb );
			float LightIntensity333 = ase_lightColor.a;
			float temp_output_383_0 = step( 0.001 , LightIntensity333 );
			float RealtimelightMask393 = temp_output_383_0;
			float lerpResult396 = lerp( 1.0 , hsvTorgb322.z , RealtimelightMask393);
			float LightColorValue315 = lerpResult396;
			float3 hsvTorgb321 = HSVToRGB( float3(hsvTorgb322.x,hsvTorgb322.y,LightColorValue315) );
			float3 LightColor136 = hsvTorgb321;
			float3 lightColor460 = LightColor136;
			float3 N92 = WorldNormal72;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ViewDir93 = ase_worldViewDir;
			float3 V92 = ViewDir93;
			half3 localSHpericalHarmonicsLightDir12 = SHpericalHarmonicsLightDir12();
			float3 temp_output_18_0 = max( localSHpericalHarmonicsLightDir12 , float3( 0,0,0 ) );
			float3 break299 = temp_output_18_0;
			float3 lerpResult395 = lerp(  ( ( break299.x + break299.y + break299.z ) - 0.0 > 0.0 ? temp_output_18_0 : ( break299.x + break299.y + break299.z ) - 0.0 <= 0.0 && ( break299.x + break299.y + break299.z ) + 0.0 >= 0.0 ? _FixedFallBackLightDir : temp_output_18_0 )  , max( _WorldSpaceLightPos0.xyz , float3( 0,0,0 ) ) , RealtimelightMask393);
			float3 normalizeResult514 = normalize( lerpResult395 );
			float3 lerpResult538 = lerp( normalizeResult514 , ( _WorldSpaceLightPos0.xyz - ase_worldPos ) , _WorldSpaceLightPos0.w);
			float3 lightDir73 = lerpResult538;
			float3 L92 = lightDir73;
			float2 uv_SpecGlossMap44 = i.uv_texcoord;
			float4 tex2DNode44 = tex2D( _SpecGlossMap, uv_SpecGlossMap44 );
			float Smoothness160 = ( tex2DNode44.a * _Glossiness );
			float clampResult46 = clamp( ( 1.0 - saturate( Smoothness160 ) ) , 0.01 , 0.99 );
			float Roughness101 = clampResult46;
			float Roughness92 = Roughness101;
			float localOrenNayarDiffuseCalc92 = OrenNayarDiffuseCalc92( N92 , V92 , L92 , Roughness92 );
			float2 uv_OcclusionMap206 = i.uv_texcoord;
			float4 tex2DNode206 = tex2D( _OcclusionMap, uv_OcclusionMap206 );
			float AmbientOcclusion193 = saturate( (( _PackedAO )?( tex2DNode206.g ):( tex2DNode206.r )) );
			float lerpResult385 = lerp( 1.0 , ase_lightAtten , temp_output_383_0);
			float LightAttenuation314 = lerpResult385;
			float DiffuseTerm183 = ( saturate( localOrenNayarDiffuseCalc92 ) * AmbientOcclusion193 * LightAttenuation314 );
			float diffTerm460 = DiffuseTerm183;
			float2 uv_MainTex48 = i.uv_texcoord;
			float3 DiffRaw247 = (( tex2D( _MainTex, uv_MainTex48 ) * _Color )).rgb;
			float3 SpecularRaw49 = (( tex2DNode44 * _SpecColor )).rgb;
			float DissableEC545 = (( _UseInBuiltEnergyConservation )?( 0.0 ):( 1.0 ));
			float3 lerpResult549 = lerp( ( DiffRaw247 * ( float3( 1,1,1 ) - SpecularRaw49 ) ) , DiffRaw247 , DissableEC545);
			float3 DiffColor551 = lerpResult549;
			float3 diffColor460 = DiffColor551;
			float3 L1 = lightDir73;
			float roughness1 = Roughness101;
			float temp_output_336_0 = ( _DIOR - 1.0 );
			float temp_output_337_0 = ( _DIOR + 1.0 );
			float CombinedIOR96 = ( ( temp_output_336_0 * temp_output_336_0 ) / ( temp_output_337_0 * temp_output_337_0 ) );
			float F01 = CombinedIOR96;
			float3 V1 = ViewDir93;
			float3 N1 = WorldNormal72;
			float localGGXSpecularCalc1 = GGXSpecularCalc1( L1 , roughness1 , F01 , V1 , N1 );
			float SpecularTerm155 = saturate( localGGXSpecularCalc1 );
			float specularTerm460 = SpecularTerm155;
			float3 SpecColor550 = SpecularRaw49;
			float3 specColor460 = SpecColor550;
			float3 indirectNormal106 = WorldNormal72;
			float smoothness361 = Smoothness160;
			float F0361 = CombinedIOR96;
			float3 V361 = ViewDir93;
			float3 N361 = WorldNormal72;
			float localFresnelCorrectedSmoothness361 = FresnelCorrectedSmoothness361( smoothness361 , F0361 , V361 , N361 );
			Unity_GlossyEnvironmentData g106 = UnityGlossyEnvironmentSetup( localFresnelCorrectedSmoothness361, data.worldViewDir, indirectNormal106, float3(0,0,0));
			float3 indirectSpecular106 = UnityGI_IndirectSpecular( data, 1.0, indirectNormal106, g106 );
			float dotResult467 = dot( WorldNormal72 , lightDir73 );
			float3 Reflections157 = ( indirectSpecular106 * ( IndirectDiff228 + ( LightColor136 * ( saturate( dotResult467 ) * LightAttenuation314 ) ) ) * AmbientOcclusion193 );
			float3 reflections460 = Reflections157;
			float roughness435 = Roughness101;
			float localSurfaceReduction435 = SurfaceReduction435( roughness435 );
			float surfaceReduction460 = localSurfaceReduction435;
			float3 localFowardBasePass460 = FowardBasePass460( indirrectDiff460 , lightColor460 , diffTerm460 , diffColor460 , specularTerm460 , specColor460 , reflections460 , surfaceReduction460 );
			float2 uv_EmissionMap217 = i.uv_texcoord;
			float3 Emssions222 = (( tex2D( _EmissionMap, uv_EmissionMap217 ) * _EmissionColor )).rgb;
			float3 diffColor541 = DiffColor551;
			float3 lightColor541 = LightColor136;
			float diffTerm541 = DiffuseTerm183;
			float3 AmbientLight520 = (unity_AmbientSky).rgb;
			float3 ambient541 = AmbientLight520;
			float specularTerm541 = SpecularTerm155;
			float3 specColor541 = SpecColor550;
			float attenuation541 = LightAttenuation314;
			float3 localForwardAddPass541 = ForwardAddPass541( diffColor541 , lightColor541 , diffTerm541 , ambient541 , specularTerm541 , specColor541 , attenuation541 );
			#ifdef UNITY_PASS_FORWARDADD
				float3 staticSwitch525 = localForwardAddPass541;
			#else
				float3 staticSwitch525 = ( localFowardBasePass460 + Emssions222 );
			#endif
			c.rgb = staticSwitch525;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18301
1920;86;1920;935;4058.76;3151.265;4.74533;True;False
Node;AmplifyShaderEditor.CommentaryNode;181;-2340.492,-2932.729;Inherit;False;4839.28;2546.523;;3;173;179;172;Given an infinte amount of time, a computer, and a PointDevice, that PointDevice will produce any possible string of characters, including working shader code;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;179;-2307.029,-2859.589;Inherit;False;1939.403;2453.414;Common Values ;18;10;228;314;385;310;393;383;312;123;171;174;178;177;175;176;499;500;563;;1,0,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;176;-2228.292,-2406.781;Inherit;False;1566.357;359.6511;Light Color And Fallback;12;136;322;321;315;327;111;332;333;396;397;495;494;;1,1,1,1;0;0
Node;AmplifyShaderEditor.LightColorNode;111;-2189.178,-2371.358;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.CommentaryNode;175;-2229.727,-2040.916;Inherit;False;1850.547;436.2612;Light Direction Calc and Fallback;15;73;28;300;299;18;12;309;394;395;498;514;538;537;530;531;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;333;-2039.605,-2368.203;Inherit;False;LightIntensity;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;12;-2214.643,-1850.659;Half;False;return float3(unity_SHAr.r,unity_SHAg.g,unity_SHAb.b)@;3;False;0;SHperical Harmonics Light Dir;True;False;0;0;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;312;-2261.942,-573.1232;Inherit;False;333;LightIntensity;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;18;-1946.984,-1851.868;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;299;-1814.082,-1885.061;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.StepOpNode;383;-2042.772,-583.0474;Inherit;False;2;0;FLOAT;0.001;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;393;-1738.287,-585.7697;Inherit;False;RealtimelightMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;172;-353.4719,-2867.406;Inherit;False;1499.016;1244.708;Data Indulge;36;219;217;218;206;72;193;5;59;160;49;208;207;44;211;213;214;48;222;247;358;359;475;477;478;484;491;492;506;507;542;543;545;546;520;516;515;;0,1,0,1;0;0
Node;AmplifyShaderEditor.Vector3Node;498;-2094.816,-1996.887;Inherit;False;Property;_FixedFallBackLightDir;Fixed Fall Back Light Dir;14;0;Create;True;0;0;False;0;False;-0.5,1,-1;-0.5,1,-1;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightPos;530;-1750.284,-1744.965;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;300;-1581.082,-1885.061;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCIf;28;-1447.296,-1946.08;Inherit;False;6;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;211;-32.63544,-2076.694;Inherit;False;Property;_Glossiness;Gloss Multiply;6;0;Create;False;0;0;False;0;False;1;0.742;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;394;-1379.608,-1683.984;Inherit;False;393;RealtimelightMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;44;-310.0225,-2221.928;Inherit;True;Property;_SpecGlossMap;Specular Gloss map;2;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMaxOpNode;309;-1278.007,-1769.342;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;207;225.6841,-2105.267;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;531;-1227.176,-1996.014;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.LerpOp;395;-1133.809,-1797.035;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;177;-1276.813,-2805.73;Inherit;False;907.3809;324.8863;IOR Masking;8;42;96;336;337;338;339;340;341;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;537;-1019.785,-1905.324;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NormalizeNode;514;-988.0845,-1797.665;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;327;-2190.216,-2260.351;Inherit;False;Constant;_Float0;Float 0;12;0;Create;True;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;59;-311.6879,-1821.671;Inherit;True;Property;_BumpMap;Normal;7;2;[NoScaleOffset];[Normal];Create;False;0;0;False;0;False;-1;None;37357ada9d4faaf40ba0ae4796bfd9c5;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;397;-2216.648,-2122.312;Inherit;False;393;RealtimelightMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RGBToHSVNode;322;-2037.964,-2296.022;Inherit;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;160;351.8799,-2105.643;Inherit;False;Smoothness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;178;-2226.047,-2809.589;Inherit;False;925.5498;396.0186;Commonly Used Values and Unit Conversion;7;161;91;45;46;101;3;93;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;42;-1112.312,-2724.73;Inherit;False;Property;_DIOR;IOR;11;0;Create;False;0;0;False;0;False;1.45;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;161;-2176.047,-2569.85;Inherit;False;160;Smoothness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;396;-1744.643,-2326.478;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;538;-779.1812,-1862.509;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldNormalVector;5;-31.51874,-1816.183;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;174;-2234.207,-1225.417;Inherit;False;1863.2;628.2128;Indirect Specular lighitng;23;467;485;482;483;106;479;473;474;465;466;471;442;157;464;361;451;363;365;362;364;489;490;522;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;336;-922.3212,-2775.34;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;72;145.273,-1720.223;Inherit;False;WorldNormal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;73;-589.4456,-1974.674;Inherit;False;lightDir;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;337;-917.3212,-2682.34;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;91;-1981.137,-2568.145;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;542;-220.3602,-2389.389;Inherit;False;Property;_SpecColor;SpecualrColoring;3;0;Fetch;False;0;0;False;0;False;1,1,1,0;0.3200942,0.4433962,0.3074492,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;315;-1400.564,-2334.669;Inherit;False;LightColorValue;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;451;-2035.569,-674.6765;Inherit;False;73;lightDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;45;-1841.27,-2567.457;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;48;-303.4719,-2817.406;Inherit;True;Property;_MainTex;Diffuse;0;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;208;2.360107,-2216.398;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;332;-1755.978,-2108.658;Inherit;False;315;LightColorValue;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;338;-805.3212,-2684.34;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;213;-216.4625,-2634.458;Inherit;False;Property;_Color;Diffuse Coloring ;1;0;Create;False;0;0;False;0;False;1,1,1,0;0.5660378,0.5660378,0.5660378,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightAttenuation;310;-2258.07,-500.8435;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;365;-2211.63,-971.4818;Inherit;False;72;WorldNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;340;-792.3212,-2779.34;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;214;44.54062,-2811.56;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;173;-356.2255,-1607.631;Inherit;False;2778.183;1173.319;Lighting Calculations ;5;170;163;216;558;180;;0,0,1,1;0;0
Node;AmplifyShaderEditor.LerpOp;385;-1887.847,-522.5204;Inherit;False;3;0;FLOAT;1;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;467;-1765.477,-690.8457;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;3;-2163.663,-2759.589;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleDivideOpNode;339;-646.3212,-2731.34;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;123;-1257.531,-546.4274;Inherit;False;72;WorldNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;206;-309.5164,-2025.14;Inherit;True;Property;_OcclusionMap;Ambient Occlusion Map;8;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.HSVToRGBNode;321;-1366.964,-2253.979;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ClampOpNode;46;-1679.645,-2569.571;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.01;False;2;FLOAT;0.99;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;543;144.6398,-2222.389;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;136;-868.0458,-2244.062;Inherit;False;LightColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;471;-1629.296,-670.3244;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;314;-1739.024,-510.4948;Inherit;False;LightAttenuation;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;171;-2233.091,-1595.196;Inherit;False;1852.855;368.396;Specualr Energy Conservation ;8;547;549;550;551;552;553;556;557;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;49;352.8947,-2225.248;Inherit;False;SpecularRaw;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;10;-1053.936,-544.9403;Inherit;False;World;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;93;-1988.744,-2731.21;Inherit;False;ViewDir;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;507;178.5654,-2816.87;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;96;-573.431,-2592.164;Inherit;False;CombinedIOR;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;163;-343.4075,-1558.343;Inherit;False;607.7173;432.4623;Oren Nayar Diffuse Calc;6;92;94;76;75;132;505;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;101;-1543.498,-2570.114;Inherit;False;Roughness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;359;157.9755,-1982.825;Inherit;False;Property;_PackedAO;PackedAO?;5;0;Create;True;0;0;False;0;False;0;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;363;-2209.977,-1105.97;Inherit;False;96;CombinedIOR;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;358;542.4113,-1952.175;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;75;-338.4075,-1505.343;Inherit;False;72;WorldNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ToggleSwitchNode;546;604.1663,-2286.796;Inherit;False;Property;_UseInBuiltEnergyConservation;Use InBuilt Energy Conservation;4;0;Create;True;0;0;False;0;False;1;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;170;-341.0559,-1115.014;Inherit;False;690.8075;410.1872;GGX specular Calc;8;155;1;153;151;74;152;154;562;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;466;-1465.554,-744.0966;Inherit;False;136;LightColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;563;-1455.755,-675.6954;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;553;-1567.112,-1553.798;Inherit;False;49;SpecularRaw;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;247;504.0695,-2817.406;Inherit;False;DiffRaw;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;132;-325.7609,-1297.88;Inherit;False;101;Roughness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;76;-307.5715,-1365.651;Inherit;False;73;lightDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;364;-2208.977,-1041.971;Inherit;False;93;ViewDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;94;-309.3635,-1435.017;Inherit;False;93;ViewDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;228;-581.0803,-509.7454;Inherit;False;IndirectDiff;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;362;-2210.977,-1166.97;Inherit;False;160;Smoothness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;465;-1464.108,-816.0131;Inherit;False;228;IndirectDiff;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;545;892.1663,-2285.796;Inherit;False;DissableEC;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;474;-1271.585,-739.7338;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;152;-319.4344,-1000.191;Inherit;False;101;Roughness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;74;-319.5758,-1068.891;Inherit;False;73;lightDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomExpressionNode;361;-1966.324,-1133.411;Inherit;False;    float dotVN = dot(V,N)@$$    float F, S@$$    F = F0 + (1.-F0) * pow((1. - dotVN),5.)@$    S = max(smoothness,F)@$    return S@;1;False;4;True;smoothness;FLOAT;0;In;;Inherit;False;True;F0;FLOAT;1;In;;Inherit;False;True;V;FLOAT3;0,0,0;In;;Inherit;False;True;N;FLOAT3;0,0,0;In;;Inherit;False;Fresnel Corrected Smoothness;True;False;0;4;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;193;681.803,-1955.729;Inherit;False;AmbientOcclusion;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;92;-95.8413,-1450.143;Inherit;False;    float3 H = normalize(V+L)@$$    float dotNL = saturate(dot(N,L))@$    float dotNV = saturate(dot(N,V))@$    float dotLH = saturate(dot(L,H))@$    float dotNH = saturate(dot(N,H))@$    float dotLH5 = pow(1.0-dotLH,5.)@$    $    float theta_r = acos(dotNV)@$	float theta_i = acos(dotNL)@$	$	float alpha = max( theta_i, theta_r)@$	float beta = min( theta_i, theta_r)@$	$	float A = 1.0 - 0.5 * Roughness / (Roughness + 0.33)@$	float B = 0.45 * Roughness / (Roughness + 0.09)@$$    return saturate(dotNL) * (A + B * sin(alpha) * tan(beta))@;1;False;4;True;N;FLOAT3;0,0,0;In;;Inherit;False;True;V;FLOAT3;0,0,0;In;;Inherit;False;True;L;FLOAT3;0,0,0;In;;Inherit;False;True;Roughness;FLOAT;0;In;;Inherit;False;Oren Nayar Diffuse Calc;True;False;0;4;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;552;-1709.112,-1374.798;Inherit;False;247;DiffRaw;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;153;-321.7637,-861.3378;Inherit;False;93;ViewDir;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;151;-322.6538,-931.5517;Inherit;False;96;CombinedIOR;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;154;-323.7637,-794.3378;Inherit;False;72;WorldNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;216;304.5815,-1502.185;Inherit;False;898.7719;208.7219;Mix Ambient Occlusion And Shadows With Diffuse;4;183;318;199;355;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;556;-1305.416,-1479.571;Inherit;False;2;0;FLOAT3;1,1,1;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;218;84.25349,-2414.985;Inherit;False;Property;_EmissionColor;Emissions Color;10;1;[HDR];Create;False;0;0;False;0;False;1,1,1,0;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;473;-1140.585,-807.7336;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;199;342.5815,-1434.685;Inherit;False;193;AmbientOcclusion;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;1;-77.02033,-974.8063;Inherit;False; float alpha = roughness*roughness@$$    L = normalize(L)@$    float3 H = normalize(V+L)@$$    float dotNL = dot(N,L)@$$    float dotLH = dot(L,H)@$    float dotNH = dot(N,H)@$	float dotVN = dot(V,N)@$$    float F, D, vis@$$    // D$    float alphaSqr = alpha*alpha@$    float denom = dotNH * dotNH *(alphaSqr-1.0) + 1.0@$    D = alphaSqr/(UNITY_PI * denom * denom)@$$    // F$    F = F0 + (1.-F0) * pow((1. - dotVN),5.)@$$    // V$    float k = alpha/2.@$    float k2 = k*k@$    float invK2 = 1.-k2@$    vis = 1./(dotLH*dotLH*invK2 + k2)@$    //vis = (1./(dotNL*(1.-k)+k))*(1./(dotVN*(1.-k)+k))@$$    float specular = dotNL * D * F * vis@$    return specular@$    //return vis@;1;False;5;True;L;FLOAT3;0,0,0;In;;Inherit;False;True;roughness;FLOAT;0;In;;Inherit;False;True;F0;FLOAT;1;In;;Inherit;False;True;V;FLOAT3;0,0,0;In;;Inherit;False;True;N;FLOAT3;0,0,0;In;;Inherit;False;GGX Specular Calc;True;False;0;5;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;217;-4.345317,-2600.215;Inherit;True;Property;_EmissionMap;Emissions Map;9;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.IndirectSpecularLight;106;-1341.303,-988.5982;Inherit;False;World;3;0;FLOAT3;0,0,1;False;1;FLOAT;1;False;2;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;442;-1781.349,-909.0334;Inherit;False;193;AmbientOcclusion;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;505;128.1459,-1445.826;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;318;343.5802,-1368.883;Inherit;False;314;LightAttenuation;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;557;-1021.416,-1425.571;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;547;-1305.918,-1311.343;Inherit;False;545;DissableEC;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.FogAndAmbientColorsNode;515;355.9618,-2719.955;Inherit;False;unity_AmbientSky;0;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;219;325.778,-2594.061;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;562;140.268,-973.2629;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;464;-721.1355,-891.5406;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;180;615.0796,-1172.297;Inherit;False;1755.332;705.8517;Final Shading Output;16;0;525;541;462;460;463;435;521;256;158;191;156;378;487;488;437;;1,0,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;355;748.0949,-1464.06;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;549;-745.9731,-1395.264;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;437;647.9745,-672.4971;Inherit;False;101;Roughness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;506;466.1077,-2601.705;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;157;-595.4038,-894.8127;Inherit;False;Reflections;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;516;605.3528,-2726.836;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;155;281.3342,-974.5178;Inherit;False;SpecularTerm;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;550;-594.7698,-1505.012;Inherit;False;SpecColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;183;888.2966,-1465.971;Inherit;False;DiffuseTerm;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;551;-592.704,-1373.839;Inherit;False;DiffColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;520;797.985,-2725.309;Inherit;False;AmbientLight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;222;674.4031,-2603.801;Inherit;False;Emssions;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;158;836.2714,-731.1542;Inherit;False;157;Reflections;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;156;824.1354,-871.3971;Inherit;False;155;SpecularTerm;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;378;836.097,-1142.493;Inherit;False;228;IndirectDiff;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;191;834.9882,-1071.744;Inherit;False;136;LightColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;488;829.6735,-1000.282;Inherit;False;183;DiffuseTerm;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;256;821.6454,-798.6514;Inherit;False;550;SpecColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;487;824.7963,-936.835;Inherit;False;551;DiffColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomExpressionNode;435;827.5398,-667.4832;Inherit;False;    float surfaceReduction@$#   ifdef UNITY_COLORSPACE_GAMMA$        surfaceReduction = 1.0-0.28*roughness*roughness*roughness@$#   else$        surfaceReduction = 1.0 / (roughness*roughness + 1.0)@$#   endif$return surfaceReduction@;1;False;1;True;roughness;FLOAT;0;In;;Inherit;False;SurfaceReduction;True;False;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;521;829.121,-603.075;Inherit;False;520;AmbientLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;558;816.2271,-537.1804;Inherit;False;314;LightAttenuation;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;460;1215.82,-1027.52;Inherit;False;return  diffColor* ((lightColor * diffTerm ) + indirrectDiff)$+ specularTerm * lightColor * specColor$+surfaceReduction * reflections * specColor@;3;False;8;True;indirrectDiff;FLOAT3;0,0,0;In;;Inherit;False;True;lightColor;FLOAT3;0,0,0;In;;Inherit;False;True;diffTerm;FLOAT;0;In;;Inherit;False;True;diffColor;FLOAT3;0,0,0;In;;Inherit;False;True;specularTerm;FLOAT;0;In;;Inherit;False;True;specColor;FLOAT3;0,0,0;In;;Inherit;False;True;reflections;FLOAT3;0,0,0;In;;Inherit;False;True;surfaceReduction;FLOAT;0;In;;Inherit;False;FowardBasePass;True;False;0;8;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;463;1458.618,-929.3352;Inherit;False;222;Emssions;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomExpressionNode;541;1219.648,-798.9043;Inherit;False;return  ((diffColor* (lightColor * diffTerm))$+ specularTerm * lightColor * specColor) * attenuation@;3;False;7;True;diffColor;FLOAT3;0,0,0;In;;Inherit;False;True;lightColor;FLOAT3;0,0,0;In;;Inherit;False;True;diffTerm;FLOAT;0;In;;Inherit;False;True;ambient;FLOAT3;0,0,0;In;;Inherit;False;True;specularTerm;FLOAT;0;In;;Inherit;False;True;specColor;FLOAT3;0,0,0;In;;Inherit;False;True;attenuation;FLOAT;0;In;;Inherit;False;ForwardAddPass;True;False;0;7;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT3;0,0,0;False;6;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;462;1643.618,-996.335;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;499;-743.3674,-551.1478;Inherit;False;3;0;FLOAT3;1,1,1;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;522;-1841.355,-774.9575;Inherit;False;520;AmbientLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;478;876.925,-1837.11;Inherit;False;fallbackReflections;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ToggleSwitchNode;491;400.0798,-2408.477;Inherit;False;Property;_EnforceFixedFallBacks;Enforce FixedFallBacks;12;0;Create;True;0;0;False;0;False;0;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;495;-1959.017,-2157.654;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;483;-1070.185,-1176.106;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;477;665.592,-1854.432;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;489;-1282.291,-871.1616;Inherit;False;492;FixedFallBacks;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;484;342.8398,-1847.283;Inherit;False;worldReflections;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;525;1785.589,-1000.262;Inherit;False;Property;_Keyword0;Keyword 0;16;0;Create;True;0;0;False;0;False;0;0;0;False;UNITY_PASS_FORWARDADD;Toggle;2;Key0;Key1;Fetch;False;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;485;-1665.961,-1147.172;Inherit;False;484;worldReflections;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldReflectionVector;475;155.8566,-1863.739;Inherit;False;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;482;-1395.938,-1173.725;Inherit;True;Property;_FallBackreflectionsCubeMap;Fixed Fall Back reflections CubeMap;13;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;None;4fabf16b4ac20454cbde66683be90c75;True;0;False;gray;LockedToCube;False;Object;-1;MipLevel;Cube;6;0;SAMPLER2D;;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;341;-996.7213,-2566.54;Inherit;False;Constant;_Float2;Float 2;13;0;Create;True;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;494;-2218.017,-2189.654;Inherit;False;492;FixedFallBacks;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;500;-1021.279,-481.6778;Inherit;False;492;FixedFallBacks;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;479;-1666.469,-1074.774;Inherit;False;float roughness = 1. - smoothness@$roughness = roughness*(1.7 - 0.7*roughness)@$$    float mip = roughness * UNITY_SPECCUBE_LOD_STEPS@$return mip@;1;False;1;True;smoothness;FLOAT;0;In;;Inherit;False;Smoothness to reflection mips ;True;False;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;490;-870.8199,-997.0234;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;492;663.0798,-2407.477;Inherit;False;FixedFallBacks;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2117.08,-1080.32;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;PointDevice/PBR High Standards/Specular Model Beta1_8 RC_1 pre release;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;333;0;111;2
WireConnection;18;0;12;0
WireConnection;299;0;18;0
WireConnection;383;1;312;0
WireConnection;393;0;383;0
WireConnection;300;0;299;0
WireConnection;300;1;299;1
WireConnection;300;2;299;2
WireConnection;28;0;300;0
WireConnection;28;2;18;0
WireConnection;28;3;498;0
WireConnection;28;4;18;0
WireConnection;309;0;530;1
WireConnection;207;0;44;4
WireConnection;207;1;211;0
WireConnection;395;0;28;0
WireConnection;395;1;309;0
WireConnection;395;2;394;0
WireConnection;537;0;530;1
WireConnection;537;1;531;0
WireConnection;514;0;395;0
WireConnection;322;0;111;1
WireConnection;160;0;207;0
WireConnection;396;0;327;0
WireConnection;396;1;322;3
WireConnection;396;2;397;0
WireConnection;538;0;514;0
WireConnection;538;1;537;0
WireConnection;538;2;530;2
WireConnection;5;0;59;0
WireConnection;336;0;42;0
WireConnection;72;0;5;0
WireConnection;73;0;538;0
WireConnection;337;0;42;0
WireConnection;91;0;161;0
WireConnection;315;0;396;0
WireConnection;45;0;91;0
WireConnection;208;0;44;0
WireConnection;208;1;542;0
WireConnection;338;0;337;0
WireConnection;338;1;337;0
WireConnection;340;0;336;0
WireConnection;340;1;336;0
WireConnection;214;0;48;0
WireConnection;214;1;213;0
WireConnection;385;1;310;0
WireConnection;385;2;383;0
WireConnection;467;0;365;0
WireConnection;467;1;451;0
WireConnection;339;0;340;0
WireConnection;339;1;338;0
WireConnection;321;0;322;1
WireConnection;321;1;322;2
WireConnection;321;2;332;0
WireConnection;46;0;45;0
WireConnection;543;0;208;0
WireConnection;136;0;321;0
WireConnection;471;0;467;0
WireConnection;314;0;385;0
WireConnection;49;0;543;0
WireConnection;10;0;123;0
WireConnection;93;0;3;0
WireConnection;507;0;214;0
WireConnection;96;0;339;0
WireConnection;101;0;46;0
WireConnection;359;0;206;1
WireConnection;359;1;206;2
WireConnection;358;0;359;0
WireConnection;563;0;471;0
WireConnection;563;1;314;0
WireConnection;247;0;507;0
WireConnection;228;0;10;0
WireConnection;545;0;546;0
WireConnection;474;0;466;0
WireConnection;474;1;563;0
WireConnection;361;0;362;0
WireConnection;361;1;363;0
WireConnection;361;2;364;0
WireConnection;361;3;365;0
WireConnection;193;0;358;0
WireConnection;92;0;75;0
WireConnection;92;1;94;0
WireConnection;92;2;76;0
WireConnection;92;3;132;0
WireConnection;556;1;553;0
WireConnection;473;0;465;0
WireConnection;473;1;474;0
WireConnection;1;0;74;0
WireConnection;1;1;152;0
WireConnection;1;2;151;0
WireConnection;1;3;153;0
WireConnection;1;4;154;0
WireConnection;106;0;365;0
WireConnection;106;1;361;0
WireConnection;505;0;92;0
WireConnection;557;0;552;0
WireConnection;557;1;556;0
WireConnection;219;0;217;0
WireConnection;219;1;218;0
WireConnection;562;0;1;0
WireConnection;464;0;106;0
WireConnection;464;1;473;0
WireConnection;464;2;442;0
WireConnection;355;0;505;0
WireConnection;355;1;199;0
WireConnection;355;2;318;0
WireConnection;549;0;557;0
WireConnection;549;1;552;0
WireConnection;549;2;547;0
WireConnection;506;0;219;0
WireConnection;157;0;464;0
WireConnection;516;0;515;0
WireConnection;155;0;562;0
WireConnection;550;0;553;0
WireConnection;183;0;355;0
WireConnection;551;0;549;0
WireConnection;520;0;516;0
WireConnection;222;0;506;0
WireConnection;435;0;437;0
WireConnection;460;0;378;0
WireConnection;460;1;191;0
WireConnection;460;2;488;0
WireConnection;460;3;487;0
WireConnection;460;4;156;0
WireConnection;460;5;256;0
WireConnection;460;6;158;0
WireConnection;460;7;435;0
WireConnection;541;0;487;0
WireConnection;541;1;191;0
WireConnection;541;2;488;0
WireConnection;541;3;521;0
WireConnection;541;4;156;0
WireConnection;541;5;256;0
WireConnection;541;6;558;0
WireConnection;462;0;460;0
WireConnection;462;1;463;0
WireConnection;499;1;10;0
WireConnection;499;2;500;0
WireConnection;478;0;477;0
WireConnection;495;0;494;0
WireConnection;495;1;397;0
WireConnection;483;0;482;0
WireConnection;484;0;475;0
WireConnection;525;1;462;0
WireConnection;525;0;541;0
WireConnection;475;0;59;0
WireConnection;482;1;485;0
WireConnection;482;2;479;0
WireConnection;479;0;361;0
WireConnection;490;0;483;0
WireConnection;490;1;106;0
WireConnection;490;2;489;0
WireConnection;492;0;491;0
WireConnection;0;13;525;0
ASEEND*/
//CHKSM=5128EE90375F5DA030B86E159911E33DC6A7776F